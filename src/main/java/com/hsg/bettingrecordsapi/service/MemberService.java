package com.hsg.bettingrecordsapi.service;

import com.hsg.bettingrecordsapi.entity.Member;
import com.hsg.bettingrecordsapi.model.member.MemberCreateRequest;
import com.hsg.bettingrecordsapi.model.member.MemberItem;
import com.hsg.bettingrecordsapi.model.member.MemberNameChangeRequest;
import com.hsg.bettingrecordsapi.model.member.MemberResponse;
import com.hsg.bettingrecordsapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember (MemberCreateRequest request) {
        Member addData = new Member();

        addData.setName(request.getName());
        addData.setDateBirth(request.getDateBirth());
        addData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(addData);
    }

    public Member getMemberId (long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public List<MemberItem> getMembers () {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList) {
            MemberItem addList = new MemberItem();
            addList.setId(member.getId());
            addList.setName(member.getName());
            addList.setDateBirth(member.getDateBirth());

            result.add(addList);
        }

        return result;
    }

    public MemberResponse getMember (long id) {
        Member detailList = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();

        response.setId(detailList.getId());
        response.setName(detailList.getName());
        response.setDateBirth(detailList.getDateBirth());
        response.setEtcMemo(detailList.getEtcMemo());

        return response;
    }

    public void putMember (long id, MemberNameChangeRequest request) {
        Member useList = memberRepository.findById(id).orElseThrow();

        useList.setName(request.getName());

        memberRepository.save(useList);
    }

    public void delMember (long id) {
        memberRepository.deleteById(id);
    }
}
