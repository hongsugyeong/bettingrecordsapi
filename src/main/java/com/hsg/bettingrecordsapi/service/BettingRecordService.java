package com.hsg.bettingrecordsapi.service;

import com.hsg.bettingrecordsapi.entity.BettingRecord;
import com.hsg.bettingrecordsapi.entity.Member;
import com.hsg.bettingrecordsapi.model.betting.BettingCreateRequest;
import com.hsg.bettingrecordsapi.model.betting.BettingItem;
import com.hsg.bettingrecordsapi.model.betting.BettingPriceChangeRequest;
import com.hsg.bettingrecordsapi.model.betting.BettingResponse;
import com.hsg.bettingrecordsapi.repository.BettingRecordsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BettingRecordService {
    private final BettingRecordsRepository bettingRecordsRepository;

    public void setBetting (Member member, BettingCreateRequest request) {

        BettingRecord addData = new BettingRecord();

        addData.setMember(member);
        addData.setDatePrize(request.getDatePrize());
        addData.setPrice(request.getPrice());

        bettingRecordsRepository.save(addData);
    }

    public List<BettingItem> getBettings () {
        List<BettingRecord> originList = bettingRecordsRepository.findAll();

        List<BettingItem> result = new LinkedList<>();

        for (BettingRecord record : originList) {
            BettingItem addList = new BettingItem();
            addList.setId(record.getId());
            addList.setMemberName(record.getMember().getName());
            addList.setDatePrize(record.getDatePrize());

            result.add(addList);
        }

        return result;
    }

    public BettingResponse getBetting (long id) {
        BettingRecord record = bettingRecordsRepository.findById(id).orElseThrow();

        BettingResponse detailList = new BettingResponse();

        detailList.setId(record.getId());
        detailList.setName(record.getMember().getName());
        detailList.setDateBirth(record.getMember().getDateBirth());
        detailList.setDatePrize(record.getDatePrize());
        detailList.setPrice(record.getPrice());

        return detailList;
    }

    public void putBetting (long id, BettingPriceChangeRequest request) {
        BettingRecord record = bettingRecordsRepository.findById(id).orElseThrow();

        record.setPrice(request.getPrice());
    }

    public void delBetting (long id) {
        bettingRecordsRepository.deleteById(id);
    }
}
