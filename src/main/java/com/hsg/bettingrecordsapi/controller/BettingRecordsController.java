package com.hsg.bettingrecordsapi.controller;

import com.hsg.bettingrecordsapi.entity.Member;
import com.hsg.bettingrecordsapi.model.betting.BettingCreateRequest;
import com.hsg.bettingrecordsapi.model.betting.BettingItem;
import com.hsg.bettingrecordsapi.model.betting.BettingPriceChangeRequest;
import com.hsg.bettingrecordsapi.model.betting.BettingResponse;
import com.hsg.bettingrecordsapi.service.BettingRecordService;
import com.hsg.bettingrecordsapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/betting")
public class BettingRecordsController {
    private final BettingRecordService bettingRecordService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setBetting (@PathVariable long memberId, @RequestBody BettingCreateRequest request) {
        Member id = memberService.getMemberId(memberId);
        bettingRecordService.setBetting(id, request);

        return "OKK";
    }

    @GetMapping("/all")
    public List<BettingItem> getBettings () {
        return bettingRecordService.getBettings();
    }

    @GetMapping("/detail/{id}")
    public BettingResponse getBetting (@PathVariable long id) {
        return bettingRecordService.getBetting(id);
    }

    @PutMapping("/change/{id}")
    public String putBetting (@PathVariable long id, @RequestBody BettingPriceChangeRequest request) {
        bettingRecordService.putBetting(id, request);

        return "OKK";
    }

    @DeleteMapping("/delete/{id}")
    public String delBetting (@PathVariable long id) {
        bettingRecordService.delBetting(id);

        return "OKK";
    }
}
