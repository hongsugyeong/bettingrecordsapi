package com.hsg.bettingrecordsapi.controller;

import com.hsg.bettingrecordsapi.entity.Member;
import com.hsg.bettingrecordsapi.model.member.MemberCreateRequest;
import com.hsg.bettingrecordsapi.model.member.MemberItem;
import com.hsg.bettingrecordsapi.model.member.MemberNameChangeRequest;
import com.hsg.bettingrecordsapi.model.member.MemberResponse;
import com.hsg.bettingrecordsapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/v1/new")
    public String setMember (@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "OKK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers () {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember (@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PutMapping("/change/name/{id}")
    public String putMember (@PathVariable long id, @RequestBody MemberNameChangeRequest request) {
        memberService.putMember(id, request);

        return "OKK";
    }

    @DeleteMapping("/delete/{id}")
    public String delMember (@PathVariable long id) {
        memberService.delMember(id);

        return "OKK";
    }
}
