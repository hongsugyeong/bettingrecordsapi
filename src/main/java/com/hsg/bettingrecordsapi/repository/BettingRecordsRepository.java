package com.hsg.bettingrecordsapi.repository;

import com.hsg.bettingrecordsapi.entity.BettingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BettingRecordsRepository extends JpaRepository<BettingRecord, Long> {
}
