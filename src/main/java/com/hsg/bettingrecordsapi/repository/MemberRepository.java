package com.hsg.bettingrecordsapi.repository;

import com.hsg.bettingrecordsapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
