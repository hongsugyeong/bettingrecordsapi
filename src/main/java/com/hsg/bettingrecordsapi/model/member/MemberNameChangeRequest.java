package com.hsg.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberNameChangeRequest {
    private String name;
}
