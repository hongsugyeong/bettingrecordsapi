package com.hsg.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String name;
    private LocalDate dateBirth;
}
