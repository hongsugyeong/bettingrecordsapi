package com.hsg.bettingrecordsapi.model.betting;

import com.hsg.bettingrecordsapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BettingItem {
    private Long id;
    private String memberName;
    private LocalDate datePrize;
}
