package com.hsg.bettingrecordsapi.model.betting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BettingPriceChangeRequest {
    private Double price;
}
