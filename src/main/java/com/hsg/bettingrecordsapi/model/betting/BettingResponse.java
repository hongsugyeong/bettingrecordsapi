package com.hsg.bettingrecordsapi.model.betting;

import com.hsg.bettingrecordsapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BettingResponse {
    private Long id;
    private String name;
    private LocalDate dateBirth;
    private LocalDate datePrize;
    private Double price;
}
