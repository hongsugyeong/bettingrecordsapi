package com.hsg.bettingrecordsapi.model.betting;

import com.hsg.bettingrecordsapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BettingCreateRequest {
    private LocalDate datePrize;
    private Double price;
}
